#unity5-demo

###Restore the database
Import unity5demo.sql

###Create an Env configuration file
Copy .env-example to .env and populate with dev credentials

###Seed the Contacts
From the command line run
```
php ./public/index.php database seed
```
