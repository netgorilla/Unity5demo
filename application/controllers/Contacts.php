<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends CI_Controller
{
    protected $contact;

    protected $contactName;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Contact_Model');

        // load Pagination library
        $this->load->library('pagination');
    }

    /**
     * List all records
     */
    public function index()
    {
        $limit_per_page = 10;
        $start_index = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $total_records = $this->Contact_Model->total();

        $config['base_url'] = base_url() . 'contacts';
        $config['page_query_string'] = true;
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;

        $config['full_tag_open'] = '<ul class="pagination" role="navigation" aria-label="Pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_tag_open'] = '<li class="pagination-previous">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="current">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="pagination-next">';
        $config['next_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);

        $data = [
            'metaTitle' => 'All Contacts',
            'content'   => 'contacts/index',
            'contacts'  => $this->Contact_Model->fetchAll($limit_per_page, $start_index),
            'links' => $this->pagination->create_links(),
        ];

        $this->load->view('templates/layout', $data);
    }

    /**
     * Show a specific record
     *
     * @param $id
     */
    public function show($id)
    {
        $this->getContact($id);

        $data = [
            'metaTitle' => 'Contact | ' . $this->contactName,
            'content'   => 'contacts/show',
            'contact'   => $this->contact,
        ];

        $this->load->view('templates/layout', $data);
    }

    /**
     * Display the view for creating a new record
     */
    public function create()
    {
        $data = [
            'metaTitle' => 'Create New Contact',
            'content'   => 'contacts/create',
            'contact'   => null,
        ];

        $this->load->view('templates/layout', $data);
    }

    /**
     * Store a new record
     */
    public function store()
    {
        $this->setValidationRules();

        if ($this->form_validation->run() == false) {

            $this->setFlashMessage('Oops! There seems to be a problem with the information you\'ve supplied. Please check below for errors...',
                'alert');

            $this->create();

        } else {
            $this->Contact_Model->save();

            $this->setFlashMessage('Contact has been successfully created', 'success');

            redirect('/contacts');

        }
    }

    /**
     * Display the view for editing an existing record
     */
    public function edit($id)
    {
        $this->getContact($id);

        $data = [
            'metaTitle' => 'Edit Contact | ' . $this->contactName,
            'content'   => 'contacts/edit',
            'contact'   => $this->contact,
        ];


        $this->load->view('templates/layout', $data);

    }

    /**
     * Update an existing record
     */
    public function update($id)
    {
        $this->getContact($id);

        $this->setValidationRules();

        if ($this->form_validation->run() == false) {

            $this->setFlashMessage('Oops! There seems to be a problem with the information you\'ve supplied. Please check below for errors...',
                'alert');

            $this->edit($id);

        } else {
            $this->Contact_Model->save($id);

            $this->setFlashMessage('Contact has been updated successfully', 'success');

            redirect('/contacts/' . $id);

        }

    }

    /**
     * Delete an existing record
     */
    public function destroy($id)
    {
        $this->getContact($id);

        // Check to make sure the delete has been confirmed
        $confirmed = $this->input->get('confirmed', "yes");

        if ($confirmed !== 'yes') {
            $data = [
                'metaTitle' => 'Delete Contact | ' . $this->contactName,
                'content'   => 'contacts/destroy',
                'contact'   => $this->contact
            ];

            $this->load->view('templates/layout', $data);
        } else {

            $this->Contact_Model->destroy($id);

            $this->setFlashMessage('Contact has been successfully removed', 'success');

            redirect('/contacts');

        }
    }

    /**
     * @param $id
     *
     * @return bool
     */
    private function getContact($id): bool
    {
        $this->contact = $this->Contact_Model->fetch($id);

        if ($this->contact) {
            $this->contactName = $this->contact['first_name'] . '  ' . $this->contact['last_name'];

            return true;
        }

        $this->contactName = '';

        return false;
    }

    private function setValidationRules()
    {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'valid_email');
        $this->form_validation->set_rules('dob', 'DOB', 'valid_date');
    }

}