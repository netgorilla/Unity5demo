<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // can only be called from the command line
        if ( ! $this->input->is_cli_request()) {
            exit('Direct access is not allowed');
        }

        // can only be run in the development environment
        if (ENVIRONMENT !== 'development') {
            exit('Wowsers! You don\'t want to do that!');
        }

        // initiate faker
        $this->faker = Faker\Factory::create();

        // load any required models
        $this->load->model('Contact_Model');
    }

    /**
     * seed local database
     */
    public function seed()
    {
        // purge existing data
        $this->truncate_db();

        // seed users
        $this->seed_contacts(250);
    }

    /**
     * seed users
     *
     * @param int $limit
     */
    private function seed_contacts($limit)
    {
        echo "seeding $limit contacts";

        // create a bunch of base buyer accounts
        for ($i = 0; $i < $limit; $i ++) {
            echo ".";

            $data = array(
                'salutation' => $this->faker->title,
                'first_name' => $this->faker->firstName,
                'last_name'  => $this->faker->lastName,
                'dob'        => $this->faker->dateTimeThisCentury->format('Y-m-d H:i:s'),
                'address'    => $this->faker->streetAddress,
                'city'       => $this->faker->city,
                'county'     => $this->faker->state,
                'postcode'   => $this->faker->postcode,
                'tel'        => $this->faker->phoneNumber,
                'email'      => $this->faker->email,
            );

            $this->Contact_Model->insert($data);
        }

        echo PHP_EOL;
    }

    private function truncate_db()
    {
        $this->Contact_Model->truncate();
    }
}