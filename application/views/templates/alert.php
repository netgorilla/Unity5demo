<?php
if ( ! empty($alertMessage = $this->session->flashdata('alertMessage'))) :
    $alertLevel = $this->session->flashdata('alertLevel'); ?>

    <div class="callout<?php echo ' ' . $alertLevel; ?>">
        <?php echo $alertMessage; ?>
    </div>
<?php endif;
