<div class="grid-x grid-margin-x">
    <div class="cell medium-4">
        <label class="<?php if (form_error('salutation')) {
            echo ' is-invalid-label';
        } ?>" for="formSalutation">Salutation
            <input class="<?php if (form_error('salutation')) {
                echo ' is-invalid-input';
            } ?>" id="formSalutation" name="salutation" type="text" value="<?php echo set_value('salutation',
                $contact['salutation']); ?>">
        </label>
        <span class="form-error<?php if (form_error('salutation')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('salutation'); ?>
    </span>
    </div>
    <div class="cell medium-4">
        <label class="<?php if (form_error('first_name')) {
            echo ' is-invalid-label';
        } ?>" for="formFirstName">First Name
            <input class="<?php if (form_error('first_name')) {
                echo ' is-invalid-input';
            } ?>" id="formFirstName" name="first_name" type="text" value="<?php echo set_value('first_name',
                $contact['first_name']); ?>">
        </label>
        <span class="form-error<?php if (form_error('first_name')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('first_name'); ?>
    </span>
    </div>
    <div class="cell medium-4">
        <label class="<?php if (form_error('last_name')) {
            echo ' is-invalid-label';
        } ?>" for="formLastName">Last Name
            <input class="<?php if (form_error('last_name')) {
                echo ' is-invalid-input';
            } ?>" id="formLastName" name="last_name" type="text" value="<?php echo set_value('last_name',
                $contact['last_name']); ?>">
        </label>
        <span class="form-error<?php if (form_error('last_name')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('last_name'); ?>
    </span>
    </div>
</div>

<div class="grid-x grid-margin-x">
    <div class="cell medium-6">
        <label class="<?php if (form_error('address')) {
            echo ' is-invalid-label';
        } ?>" for="formAddress">Address
            <textarea class="<?php if (form_error('address')) {
                echo ' is-invalid-input';
            } ?>" name="address" id="formAddress" cols="30" rows="5"><?php echo set_value('address',
                    $contact['address']); ?></textarea>
            <span class="form-error<?php if (form_error('address')) {
                echo ' is-visible';
            } ?>">
        <?php echo form_error('address'); ?>
    </span>
        </label>

        <label class="<?php if (form_error('city')) {
            echo ' is-invalid-label';
        } ?>" for="formCity">City
            <input class="<?php if (form_error('city')) {
                echo ' is-invalid-input';
            } ?>" name="city" id="formCity" type="text" value="<?php echo set_value('city',
                $contact['city']); ?>" required>
        </label>
        <span class="form-error<?php if (form_error('city')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('city'); ?>
    </span>

        <label class="<?php if (form_error('county')) {
            echo ' is-invalid-label';
        } ?>" for="formCounty">County
            <input class="<?php if (form_error('county')) {
                echo ' is-invalid-input';
            } ?>" name="county" id="formCounty" type="text" value="<?php echo set_value('county',
                $contact['county']); ?>">
        </label>
        <span class="form-error<?php if (form_error('county')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('county'); ?>
    </span>

        <label class="<?php if (form_error('postcode')) {
            echo ' is-invalid-label';
        } ?>" for="formPostCode">Post Code
            <input class="<?php if (form_error('postcode')) {
                echo ' is-invalid-input';
            } ?>" name="postcode" id="formPostCode" type="text" value="<?php echo set_value('postcode',
                $contact['postcode']); ?>">
        </label>
        <span class="form-error<?php if (form_error('postcode')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('postcode'); ?>
    </span>
    </div>
    <div class="cell medium-6">
        <label class="<?php if (form_error('dob')) {
            echo ' is-invalid-label';
        } ?>" for="formTel">DOB
            <input class="<?php if (form_error('dob')) {
                echo ' is-invalid-input';
            } ?>" name="dob" id="formDOB" type="date" value="<?php echo set_value('dob', $contact['dob']); ?>">
        </label>
        <span class="form-error<?php if (form_error('dob')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('dob'); ?>
    </span>

        <label class="<?php if (form_error('tel')) {
            echo ' is-invalid-label';
        } ?>" for="formTel">Tel
            <input class="<?php if (form_error('tel')) {
                echo ' is-invalid-input';
            } ?>" name="tel" id="formTel" type="text" value="<?php echo set_value('tel', $contact['tel']); ?>">
        </label>
        <span class="form-error<?php if (form_error('tel')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('tel'); ?>
    </span>

        <label class="<?php if (form_error('email')) {
            echo ' is-invalid-label';
        } ?>" for="formEmail">Email
            <input class="<?php if (form_error('email')) {
                echo ' is-invalid-input';
            } ?>" name="email" id="formEmail" type="email" value="<?php echo set_value('email', $contact['email']); ?>">
        </label>
        <span class="form-error<?php if (form_error('email')) {
            echo ' is-visible';
        } ?>">
        <?php echo form_error('email'); ?>
    </span>
    </div>
</div>
