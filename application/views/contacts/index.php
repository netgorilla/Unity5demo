<div class="grid-x">
    <div class="cell text-right controls--container">
        <a class="button hollow" href="/contacts/create"><i class="fas fa-plus-circle"></i> &nbsp; Add New Contact</span></a>
    </div>
</div>

<table>
    <thead>
    <tr>
        <th>Name</th>
        <th class="hide-for-small-only">Tel</th>
        <th class="hide-for-small-only">Email</th>
        <th class="text-center">View</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($contacts as $contact): ?>
        <tr>
            <td>
                <a href="/contacts/<?php echo $contact['id']; ?>"><?php echo trim($contact['salutation'] . ' ' . $contact['first_name'] . ' ' . $contact['last_name']); ?></a>
            </td>
            <td class="hide-for-small-only"><a href="tel:<?php echo $contact['tel']; ?>"><?php echo $contact['tel']; ?></a></td>
            <td class="hide-for-small-only"><a href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a></td>
            <td class="text-center"><a class="" href="/contacts/<?php echo $contact['id']; ?>"><i class="fas fa-address-card"></i> <span class="show-for-sr"> View Details</span></a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="text-center">
    <?php if (isset($links)) :
        echo $links;
    endif ?>
</div>
