<div class="text-center">
    <strong>Are you sure you want to delete '<?php echo $contact['first_name'] . '  ' . $contact['last_name'] ?>'?!?</strong>
</div>
<hr>
<div class="grid-x grid-margin-x">
    <div class="cell small-6 text-right">
        <a class="button hollow secondary" href="/contacts/<?php echo $contact['id']; ?>">No!</a>
    </div>
    <div class="cell small-6">
        <a href="/contacts/<?php echo $contact['id']; ?>/delete?confirmed=yes" class="button alert" type="submit">Yes</a>
    </div>
</div>

