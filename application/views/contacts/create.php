<h1>Add a new Contact</h1>

<form data-abide novalidate method="post" action="/contacts">

    <?php $this->load->view('contacts/_form'); ?>

    <hr>

    <div class="grid-x">
        <div class="cell small-6">
            <a class="button hollow secondary" href="/contacts">Cancel</a>
        </div>
        <div class="cell small-6 text-right">
            <button class="button" type="submit" value="Submit">Save</button>
        </div>
    </div>

</form>