<?php if ($contact === null) : ?>
    <h1>Contact Not Found</h1>
    <hr>
    <div class="grid-x">
        <div class="cell small-2">
            <a class="button hollow secondary" href="/contacts">Back</a>
        </div>
    </div>
<?php else : ?>
    <h1><?php echo trim($contact['salutation'] . ' ' . $contact['first_name'] . ' ' . $contact['last_name']); ?></h1>

    <div class="grid-x grid-margin-x">
        <div class="cell medium-6">
            <div class="contact--label">Address</div>
            <div class="contact--details"><?php echo nl2br($contact['address']); ?></div>
            <div class="contact--label">City</div>
            <div class="contact--details"><?php echo $contact['city']; ?></div>
            <div class="contact--label">County</div>
            <div class="contact--details"><?php echo $contact['county']; ?></div>
            <div class="contact--label">Post Code</div>
            <div class="contact--details"><?php echo $contact['postcode']; ?></div>
        </div>
        <div class="cell medium-6">
            <div class="contact--label">DOB</div>
            <div class="contact--details"><?php if ($contact['dob'] !== null) {
                    echo date('d F Y', strtotime($contact['dob']));
                } ?></div>
            <div class="contact--label">Tel</div>
            <div class="contact--details">
                <a href="tel:<?php echo $contact['tel']; ?>"><?php echo $contact['tel']; ?></a></div>
            <div class="contact--label">Email</div>
            <div class="contact--details">
                <a href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a></div>
        </div>
    </div>

    <hr>

    <div class="grid-x">
        <div class="cell small-3">
            <a class="button hollow secondary" href="/contacts">Back</a>
        </div>
        <div class="cell small-2">
            <a class="button" href="/contacts/<?php echo $contact['id']; ?>/edit"><i class="fi-list"></i>
                <span>Edit</span></a>
        </div>
        <div class="cell small-7">
            <a class="button hollow alert float-right" href="/contacts/<?php echo $contact['id']; ?>/delete">Delete</a>
        </div>
    </div>
<?php endif;