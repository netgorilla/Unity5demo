<h1>Edit '<?php echo $contact['first_name'] . ' ' . $contact['last_name']; ?>'</h1>

<form data-abide novalidate method="post" action="/contacts/<?php echo $contact['id']; ?>">

    <?php $this->load->view('contacts/_form'); ?>

    <hr>

    <div class="grid-x">
        <div class="cell small-6">
            <a class="button hollow secondary" href="/contacts/<?php echo $contact['id']; ?>">Cancel</a>
        </div>
        <div class="cell small-6">
            <button class="button float-right" type="submit" value="Submit">Save</button>
        </div>
    </div>

</form>
