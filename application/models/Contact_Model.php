<?php

class Contact_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function fetch($id)
    {
        // Exclude deleted records
        $this->db->where('deleted_at', null);

        // Fetch a specific record
        $query = $this->db->get_where('contacts', array('id' => $id));

        return $query->row_array();
    }

    public function fetchAll($limit, $start)
    {
        // Exclude deleted records
        $this->db->where('deleted_at', null);

        // Order by last name
        $this->db->order_by('last_name');
        
        $this->db->limit($limit, $start);
        $query = $this->db->get('contacts');

        return $query->result_array();
    }
    
    public function total()
    {
        return $this->db->count_all('contacts');
    }

    /**
     * @param null $id
     *
     * @return mixed
     */
    public function save($id = null)
    {
        $data = $this->getData();

        if ($id === null) {
            // Add a new record
            return $this->db->insert('contacts', $data);

        } else {
            // Update an existing record
            $this->db->where('id', $id);

            return $this->db->update('contacts', $data);
        }
    }

    public function destroy($id)
    {
        $this->db->where('id', $id);

        return $this->db->update('contacts', ['deleted_at' => date('Y-m-d H:i:s',time())]);
    }

    public function insert($data)
    {
        return $this->db->insert('contacts', $data);
    }

    private function getData()
    {
        return [
            'salutation'  => $this->input->post('salutation'),
            'first_name'  => $this->input->post('first_name'),
            'middle_name' => $this->input->post('middle_name'),
            'last_name'   => $this->input->post('last_name'),
            'dob'         => ($this->input->post('dob') == '') ? null : $this->input->post('dob'),
            'address'     => $this->input->post('address'),
            'city'        => $this->input->post('city'),
            'county'      => $this->input->post('county'),
            'postcode'    => $this->input->post('postcode'),
            'tel'         => $this->input->post('tel'),
            'email'       => $this->input->post('email'),
        ];
    }

    public function truncate()
    {
        $this->db->truncate('contacts');
    }
}